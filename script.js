// Создать функцию, которая будет принимать на вход массив и опциональный второй аргумент parent - DOM-элемент, к которому будет прикреплен список (по дефолту должен быть document.body).
// // Каждый из элементов массива вывести на страницу в виде пункта списка;
// // Используйте шаблонные строки и метод map массива для формирования контента списка перед выведением его на страницу;
// //
// // Примеры массивов, которые можно выводить на экран:
// //     ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// // ["1", "2", "3", "sea", "user", 23];
// //

const root = document.querySelector(".house");
const createArrayFunction = (array, parent = document.body) => {
   const transform = array.map(item =>{
       return `<li>${item}</li>`
   } )
    const stringLi = transform.join(" ");
    return parent.insertAdjacentHTML("afterbegin",`<ul>${stringLi}</ul>`)
};

createArrayFunction(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"], root);


